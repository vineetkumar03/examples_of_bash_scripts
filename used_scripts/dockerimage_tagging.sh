#!/bin/bash
# pull docker image and push it registry
# Please insure that no old images in repository
# Create all file and directories mentioned in first five lines


imagefile=/home/vineet/script/image_name.txt
logfile=/home/vineet/script/imagename.log
imagelist=/home/vineet/script/imagelist.txt
imagelogfile=/home/vineet/script/saveimage.log
taggedlogfile=/home/vineet/script/taggedimage.log
REGISTRY=chub.cloud.gov.in/mod1c0/


while read image; do
   echo "Adding Images $image"
   docker pull $image
   echo "Downloaded image from dokcerhub | `date` |" >> $logfile
done <$imagefile

docker images | sed '1d' | awk '{ print $1":"$2 }' > $imagelist

while read image; do
   echo "Tagging Images $image"
   echo "$REGISTRY$image" >> tag.txt
    docker tag $image $REGISTRY$image
    echo "Downloaded image from $image to $REGISTRY$image | `date` |" >> $taggedlogfile
done <$imagelist

  while read image; do
   echo "Tagging Images $image"
   echo "$REGISTRY$image" >> tag.txt
   docker push $image
   echo "Downloaded image from $image to $REGISTRY$image | `date` |" >> $taggedlogfile
done < tag.txt

#!/bin/sh
#For taking backup
# create dir db_backup: mkdir -p /home/db_backup/
DIR=/home/db_backup/
DATESTAMP=$(date +%d-%m-%y-%H-%M)
# remove backups older than $DAYS_KEEP
DAYS_KEEP=7
find ${DIR}* -mtime +$DAYS_KEEP -exec rm -f {} \; 2> /dev/null
# Find Container ID where postgres is running with ancestor=<image name with tag>
CONTAINER_ID=$(docker ps -q --filter ancestor=10.10.10.10:5000/postgres:10.1)
# DATABBASE name want to take backup
DB=middleware_production_db
# Destination Path
FILENAME=${DIR}${DB}-${DATESTAMP}.sql
#echo $FILENAME
#echo $CONTAINER_ID
#docker exec -t ${CONTAINER_ID} pg_dump -U postgres -d ${DB} > ${FILENAME}
echo -n "POSTGRESQL_PASSWORD" | docker exec -i ${CONTAINER_ID} pg_dump -U postgres -W -d ${DB} > ${FILENAME} # If usename and database has diffrent names
#!/bin/bash
# pull docker image and push it registry
# Please insure that no old images in repository
# Create all file and directories mentioned in first five lines


imagefile=/home/vineet/script/image_name.txt
logfile=/home/vineet/script/imagename.log
imagelist=imagelist.txt
imagelogfile=/home/vineet/script/saveimage.log
taggedlogfile=/home/vineet/script/taggedimage.log
REGISTRY=chub.cloud.gov.in/service-cloud-dev/

while read image; do
     echo "Tagging Images $image"
     ima2=$(echo "$image" | awk -F '/' '{print $NF}')
#   echo $REGISTRY$ima2
     echo "$REGISTRY$ima2" >> tag2.txt
   docker tag $image $REGISTRY$ima2
#   echo "Downloaded image from $image to $REGISTRY$image | `date` |" >> $taggedlogfile
done < imagelist2.txt

while read image; do
   echo "Pushing Images $image"
#   echo "$REGISTRY$image" >> tag.txt
   docker push $image
#   echo "Downloaded image from $image to $REGISTRY$image | `date` |" >> $taggedlogfile
done < tag2.txt

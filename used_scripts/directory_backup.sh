#!/bin/sh

#For taking backup
SOURCE=/home/gitlab/
DIR=/home/data_backup/
NAME=data
DATESTAMP=$(date +%d-%m-%y-%H-%M)
# remove backups older than $DAYS_KEEP
DAYS_KEEP=7
find ${DIR}* -mtime +$DAYS_KEEP -exec rm -f {} \; 2> /dev/null
# create backups securely
umask 006
# New file name
FILENAME=${DIR}${NAME}-${DATESTAMP}
#rsync -arzvp ${SOURCE}${NAME} ${FILENAME} 
#cp -R ${SOURCE}${NAME} ${FILENAME}
tar -cpzf ${FILENAME}.tar.gz ${SOURCE}${NAME}

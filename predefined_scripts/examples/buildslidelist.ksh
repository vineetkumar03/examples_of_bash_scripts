#!/bin/ksh
#
# $Header: /afs/northstar/ufac/richard/projects/class-web-builder/RCS/buildslidelist,v 1.12 2005/03/19 05:15:13 richard Exp $
#
# Create a slide list suitable for a menu frame, from the named .html files.
# The "ID" tags for the index list are not derived from the file names, but from
# the numbers in the "slide.order" file, which is assumed to exist.
#
# This script is used in conjunction with buildhtml and buildslidelist.
#
# 2001/10 Richard Brittain, Dartmouth College

# Solaris has two versions of grep - make sure we get the right one by tweeking $PATH
PATH=/usr/xpg4/bin:$PATH; export PATH

# Make sure we get these only from the .conf file
unset author keywords description

# Read in the config file setting various optional features.  Look only in the current directory
[[ -r ./buildhtml.conf ]] && . ./buildhtml.conf

# Default is to make links to a different frame, named "mainplusnav"
# Option -n = no-frames version - links do not reference a different target.
# Option -h = suppress headers - don't put out the 'printable' etc. links at top. (used in the printable pages).  This
#             creates a .htinc - includable HTML, no <HEAD>,<BODY> etc.
# Option -e = "expanded" list, with subheadings from the <H> tags of the pages.
target="target=mainplusnav"
headers=yes
frames=yes
expanded=
while getopts nhe o ; do
   case $o in
   n) target= ; frames= ;;
   h) headers= ;;
   e) expanded=yes ;;
   esac
done
shift $OPTIND-1

# The introduction page is $1 - special treatment
intro=$1; shift

# Set body background variable

# <BODY BACKGROUND=background> image or colour.  Default is white, special value "none"
# omits the tag and lets the browser default be used
body_bg="${body_bg:-white}"
case $body_bg in
(*.gif|*.jpg) 
   # Assume it is an image
   bodytag="background=$body_bg" ;;
none)
   # We don't want any background
   bodytag= ;;
*)
   # Assume it is a colour - need a different syntax
   bodytag="BGCOLOR=$body_bg" ;;
esac

doctype=${doctype:-"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">"}

if [[ -n "$headers" ]]; then
   # Generate standard <HEAD>:

   print "$doctype"
   print "<html>"
   print "<head>"
   print "<!-- This was created automatically by buildhtml - do not edit -->"
   [[ -n "$author" ]]      && print '<meta name="AUTHOR" content="'"$author"'">'
   [[ -n "$keywords" ]]    && print '<meta name="keywords" content="'"$keywords"'">'
   [[ -n "$description" ]] && print '<meta name="description" content="'"$description"'">'
   if [[ -n "$frames" ]]; then
      # The "framed" slide list has a standard title (probably won't ever be seen though)
      ftitle="Class slide list"
   else
      # The "noframes" slide list needs a better title for the site as a whole
      # Generate the slide list TITLE from the TITLE of $intro.  Tag must be a single line.
      file="$intro"
      ftitle=$(grep -i '^ *<TITLE' $file | sed -e 's/.*<[tT][iI][tT][lL][eE]>//' -e 's/<\/[tT][iI][tT][lL][eE]>.*//')
   fi
   print "<title>$ftitle</title>"
   print "</head>"

   print "<body $bodytag>"

   # Generate standard links for preface

   if [[ -z "$frames" ]]; then
      # Non-framed header with title and links for text-only, printable version, framed version
      print '<h1>'"$ftitle"'</h1>'
      print '<table width=100%>'
      print '<tr>'
      print '  <td align="left"><font size=-1><a href="/cgi-bin/betsie.cgi">Text-only</a></font></td>'
      print '  <td align="center"><font size=-1><a href="print_pages.shtml" target=_top>Printable</a></font></td>'
      print '  <td align="right"><font size=-1><a href="index.html" target=_top>Framed version</a></font></td>'
      print '</tr>'
      print '</table>'
   else
      # Framed header with links for text-only, printable version, framed and frameless versions
      print '<table width=100%>'
      print '<tr>'
      print '  <td align="left"><font size=-1><a href="/cgi-bin/betsie.cgi">Text-only</a></font></td>'
      print '  <td align="right"><font size=-1><a href="print_pages.shtml" target=_top>Printable</a></font></td>'
      print '</tr>'
      print '<tr>'
      print '  <td align="left"><font size=-1><a href="index.html" target=_top>Frames</a></font></td>'
      print '  <td align="right"><font size=-1><a href="'$intro'" target=_top>No Frames</a></font></td>'
      print '</tr>'
      print '</table>'
   fi
   print "<hr>"

   # The first two links are fixed - Introduction ==> $intro (first argument)
   #                                 Research Computing ==> ~rc
   #
   print '<a href="'$intro'#top" '$target'> Introduction </a><br>'
   print '<br>'
   print '<a href="http://www.dartmouth.edu/~rc" $target> Research Computing Home</a><br>'

   # Generate the link to the "expanded" or "contracted version as needed (and framed/frameless too)
   if [[ -z "$expanded" ]]; then
      # This is the "non-expanded" version - make a link to the expanded version.  There is never a target for this link.
      if [[ -n "$frames" ]]; then 
         print '<a href="slide_list_expanded.html"><img src="right.gif" border=0 height=15 width=15 alt="Expand sections"></a>'
      else
         print '<a href="slide_list_noframes_expanded.html"><img src="right.gif" border=0 height=15 width=15 alt="Expand sections"></a>'
      fi
   else
      # This is the "expanded" version - make a link back to the non-expanded version
      if [[ -n "$frames" ]]; then 
         print '<a href="slide_list.html"><img src="down.gif" border=0 height=15 width=15 alt="Contract sections"></a>'
      else
         print '<a href="slide_list_noframes.html"><img src="down.gif" border=0 height=15 width=15 alt="Contract sections"></a>'
      fi
   fi
   print "<br>"
else
   # No headers - skip all standard HTML preface, but put out a <HR> and a newpage flag for the printed output
   print '<hr>'
   print '<p align="center" class="newpage">Table of Contents</p>'
fi

# Generate the main body of the Table of Contents
# A Table for the numbers and Titles looks much better in normal mode, but doesn't look so good 
# under Betsie.
print "<table>"

# The links are generated from the named arguments, by extracting the <TITLE>
# The File ID tag is extracted from the order number in the slide.order file.

for file in "$@"; do
   if [[ ! -r $file ]]; then
      print "$file unreadable - skipping"
      continue
   fi
   # Generate the index string from the TITLE tag.  Tag must be a single line.
   # We could also grab the first H1 tag instead.
   ftitle=$(grep -i '^ *<TITLE' $file | sed -e 's/.*<[tT][iI][tT][lL][eE]>//' -e 's/<\/[tT][iI][tT][lL][eE]>.*//')
   
   case $file in
   *.html)  html=html  ;;
   *.shtml) html=shtml ;;
   *.htm)   html=htm   ;;
   *)       html=      ;;
   esac

   # Generate the index number from the slide.order list. 
   fid=$(grep " $file\$" slide.order | awk '{print $1}')
   # If that didn't work, just strip the .html from the file name and use that.
   [[ -z $fid ]] && fid=${file%.$html}

   # Explicitly code the target frame for each link
   # For the 3-frame setup, we need to make links to the *.frameset.html files, but we've been
   # given the plain .html filenames, so generate the new names. href=${file%.$html}.frameset.html
   # For the 2-frame setup, just link to the .html files, with the appropriate target.
   print "<tr><td align="right">$fid.</td><td><a href=$file#top $target>$ftitle</a></td></tr>"

   if [[ -n "$expanded" ]]; then
      # Make indented sub-categories based on the named anchors in the HTML file
      # Skip the H1 tags, because every file will have one of those.
      # Note that this makes the slide list file dependant on the _contents_ of the slides,
      # not just the ordering of the slides (changes the Makefile rules)
      # Leading and trailing spaces in the tag values will not work, so they should be excluded 
      # when creating the tags.
      if grep -q '<a name="L[2-5]-' $file; then
         # This code actually lets us expand a single item if we wish - may use this later
         print '<!--#if expr="${QUERY_STRING} = '${file%.$html}'" -->'
         print "<tr><td></td><td><small>"
         grep '<a name="L[2-5]-' $file | sed -e's!^.*name="L\([2-5]\)-\([^"]*\)".*$!\1 \2!' | \
            while read level tag; do
            ## print -u2 level=$level tag=\"$tag\" 
            # crude indentation based on subheading level
            case $level in
            2) print -n "&nbsp;" ;;
            3) print -n "&nbsp;&nbsp;" ;;
            4) print -n "&nbsp;&nbsp;&nbsp;" ;;
            5) print -n "&nbsp;&nbsp;&nbsp&nbsp;" ;;
            esac
            print "<a href=\"$file#L$level-$tag\" $target>$tag</a><br>"
         done
         print "</small></td></tr>"
         print "<!--#endif -->"
      fi
   fi
done

print "</table>"
# Could put additional navigation at bottom of index, but these have been moved to every content page
# for better action in 'frameless' mode.
if [[ -n "$headers" ]]; then
   print "</body> "
   print "</html>"
fi
exit 0

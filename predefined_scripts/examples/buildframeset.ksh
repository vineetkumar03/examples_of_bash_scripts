#!/bin/ksh
#
# $Header: /afs/northstar/ufac/richard/projects/class-web-builder/RCS/buildframeset,v 1.9 2005/03/18 19:23:07 richard Exp $
#
# Build a frameset page for a class web site.  Usually this is put in index.html.
# This script is used in conjunction with buildhtml and buildslidelist.
#
# Optional -bBASE defines the base URL to use.  
# e.g. -b "http://www.dartmouth.edu/${PWD#/data/public/}/"
# If no baseurl is set, skip it.  We don't really need to set this 
# and it makes it easier to move the whole site around without it.
#
# Expected argument ($1) is the introduction page.
#
# 2001/10 Richard Brittain, Dartmouth College

# Solaris has two versions of grep - make sure we get the right one by tweeking $PATH
PATH=/usr/xpg4/bin:$PATH; export PATH

# Make sure we get these only from the .conf file
unset author keywords description

# Read in the config file setting various optional features.  Look only in the current directory
# This can preset several variables used later in this script.
[[ -r ./buildhtml.conf ]] && . ./buildhtml.conf
frametype=${frametype:-"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Frameset//EN\" \"http://www.w3.org/TR/REC-html40/frameset.dtd\">"}

baseurl=
while getopts b: o ; do
   case $o in
   b) baseurl=$OPTARG;;
   esac
done
shift $OPTIND-1

# Get the "class" title from the TITLE of the introduction page
# We'll use this in the printable version
intro=$1
if [[ -r "$intro" ]] ; then
   ctitle=$(grep -i '<TITLE' $intro | sed -e 's/.*<[tT][iI][tT][lL][eE]>//' -e 's/<\/[tT][iI][tT][lL][eE]>.*//')
else
   # Oops - well just use a generic title
   ctitle="Online Class Notes"
fi

print "$frametype"
print '<html>'
print '<!-- This was created automatically by buildhtml - do not edit -->'
[[ -n "$author" ]]      && print '<meta name="AUTHOR" content="'"$author"'">'
[[ -n "$keywords" ]]    && print '<meta name="keywords" content="'"$keywords"'">'
[[ -n "$description" ]] && print '<meta name="description" content="'"$description"'">'
print '<head>'
print '<title>'$ctitle'</title>'
if [[ ! -z "$baseurl" ]]; then
  print '<base href="'$baseurl'">'
fi

# Use .html for the slide lists -- no subtopic expansion unless requested

print '<frameset cols="240, *">'
print '   <noframes>'
print '      These pages are best viewed with a frames-capable browser.<br>'
print '      <a href="print_pages.shtml">Printable version (single file)</a>'
print '      <a href="slide_list_noframes.shtml">No frames version</a>'
print '   </noframes>'
print '   <frame src="slide_list.html" name="menu" >'
print '   <frame src='"$intro"' name="mainplusnav">'
print '</frameset>'
print '</head>'
print '</html>'
exit 0

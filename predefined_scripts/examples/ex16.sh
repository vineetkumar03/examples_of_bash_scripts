#!/bin/sh
# Example 16

# Uses ex13.sh to generate some output and give us an 
# exit status to capture.

# Get the exit status of ex13 into ex13stat. 
# stderr of ex13 is processed by the pipe, stdout
# is left alone.

# Save a copy of stdout
exec 3>&1

# Run a subshell, with 4 copied to 1 so we get it in stdout.  
# Capture the output in backtics`
# ex13stat=`(    ) 4>&1`

# In the subshell, run another subshell to execute ex13, and 
# echo the status code to 4
# (./ex13.sh; echo $? >&4)

# stdout from the inner subshell is directed to the original stdout (3)
# stderr is passed into the pipe for further processing.
# stdout from the pipe is redirected back to stderr

# Close the extra descriptors before running the commands
exec 3>&1
ex13stat=`((./ex13.sh 2>&1 1>&3 3>&- 4>&- ; echo $? >&4) | \
  sed s/err/ERR/ 1>&2 3>&- 4>&- ) 4>&1`

echo Last command status=$?
echo ex13stat=$ex13stat


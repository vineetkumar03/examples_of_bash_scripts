#!/bin/sh
# Add in the magic postscript preface to perform
# duplex printer control for Xerox docuprint.

# To have this script send the files directly to the printer, use 
# a subshell to collect the output of the two 'cat' commands.

## (
cat << EOP
%!PS
%%BeginFeature: *Duplex DuplexTumble
<</Duplex true /Tumble false>> setpagedevice
%%EndFeature
EOP
cat "$@"
## ) | lpr

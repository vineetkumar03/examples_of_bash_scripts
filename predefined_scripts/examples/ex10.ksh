#!/bin/ksh
# This example uses a locally written tool for Dartmouth Name Directory lookups

# Start the dndlookup program as a coprocess
# Tell it to output only the canonical full name, and to not print multiple matches
dndlookup -fname -u |&

# move the input/output streams so we 
# can use other coprocesses too
exec 4>&p
exec 5<&p

echo "Name file contents:"
cat namefile
echo

# read the names from a file "namefile"
while read uname; do
  print -u4 $uname
  read  -u5 dndname
  case $dndname in
  *many\ matches*)
    # handle case where the name wasn't unique
    print "Multiple matches to \"$uname\" in DND"
    ;;
  *no\ match*)
    # handle case where the name wasn't found
    print "No matches to \"$uname\" in DND"
    ;;
  *)
    # we seem to have a hit - process the
    # canonical named retrieved from dndlookup
    print "Unique DND match: full name for \"$uname\" is \"$dndname\""
    ;; 
  esac
  sleep 2
done < namefile

# We've read all the names, but the coprocess
# is still running.  Close the pipe to tell it
# we have finished.
exec 4>&-

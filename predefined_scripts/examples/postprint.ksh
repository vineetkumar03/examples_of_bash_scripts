#!/bin/ksh
#
# Wrapper script for postprint on Solaris
#  
# Pass files through the Solaris postprint utility, 
# but only if they are not already postscript.  
# Output is then piped directly to lp or lpr
#
# We do not process any files unless a default printer 
# has been selected.  The search list is $LPDEST, 
# $PRINTER, the _default line in $HOME/.printers
# A printer may also be specified by option "-P printer"
#
# Any other options are passed on to postprint.  
# This script knows about the postprint # options so 
# it can skip over them and get to the filenames
# 
# PROBLEMS
# - cat multiple postscript files into lpr is NOT 
#  the same as passing multiple filenames to postprint
#  and piping the output to lpr.  Postprint only generates
#  one header, then multiple sets of data.  
#  This is why we need to insert each named file into 
#  the print queue directly.
#
# 2003/10/24 RB

# Gather up the program options.
ppopts=
printer=
while getopts :c:f:l:m:n:o:p:r:s:t:x:y:P: opt
do
  case $opt in
  P)  
     printer=$OPTARG  ;;
  @(c|f|l|m|n|o|p|r|s|t|x|y)) 
     ppopts="${ppopts} -${opt}$OPTARG" ;;
  :) 
     print -u2 "$0: option \"-$OPTARG\" requires a value"
     exit 2 ;;
  \?) 
     print -u2 "$0: no such option \"-$OPTARG\""
     exit 2 ;;
  esac
done
shift OPTIND-1

# Make sure we have a preferred printer set 
# - don't rely on a system default.
printer=${printer:-$LPDEST}
printer=${printer:-$PRINTER}
printer=${printer:-$(grep '^_default' $HOME/.printers 2>/dev/null|awk '{print $2}')}
if [[ -z "$printer" ]]; then
  print -u2 "$0: no preferred printer detected in LPDEST, PRINTER or .printers file"
  exit 3
fi

# Command to put stdin into the print queue.
# Use lpr rather than lp because it is less chatty
# printcmd="/usr/bin/lp -d$printer"
printcmd="/usr/ucb/lpr -P$printer"

# Command to convert plain text to postscript.
# Use full path, since postprint, isn't in the path
ppcmd="/usr/lib/lp/postscript/postprint $ppopts"

if [[ $# -gt 0 ]]; then
  # we have one or more filenames on the command line
  print -u2 -n "$0: sending to $printer: "
  for file in "$@"; do
    if [[ -f $file && -r $file ]]; then
      # we have a real file
      print -u2 -n "$file "
      read line < $file
      case $line in
        %!*) cat $file  | $printcmd ;;
        *)   $ppcmd $file | $printcmd ;;
      esac
    elif [[ $file = "-" ]]; then
      # process stdin
      print -u2 -n "(stdin) "
      read line
      case $line in
        %!*) ( print -R $line; cat; ) | $printcmd ;;
        *) ( print -R $line; cat; ) | $ppcmd | $printcmd ;;
      esac
    else
      # invalid filename specified - non-fatal error; process remaining files
      print -u2 "\n$0: invalid filename \"$file\" -- skipping"
      continue
    fi
  done
  print -u2
else
  # no filenames specified -- process stdin
  read line 
  case $line in
    %!*) ( print -R $line; cat; ) |  $printcmd ;;
    *) ( print -R $line; cat; ) | $ppcmd | $printcmd ;;
  esac
fi

exit 0

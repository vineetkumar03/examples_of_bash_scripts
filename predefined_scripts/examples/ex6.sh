#!/bin/sh
# sh version

vprint()
{
       # Print or not depending on global "$verbosity"
       # Change the verbosity with a single variable.
       # Arg. 1 is the level for this message.
       level=$1; shift
       if [[ $level -le $verbosity ]]; then
          echo "$@"
       fi
}

verbosity=4
echo "verbosity=$verbosity"
vprint 1 This message will appear
vprint 3 This only appears if verbosity is 3 or higher

die()
{
       # Print an error message and exit with given status
       # call as: die status "message" ["message" ...]
       exitstat=$1; shift
       for i in "$@"; do
          echo "$i" >&2
       done
       exit $exitstat
}

echo "Testing die function - exit status will be 4"

die 4 "$0: file not writeable" "check permissions"

#!/bin/ksh

echo Today is `date`

file=/etc/hosts
echo The file $file has $(wc -l < $file) lines

hostname -s > myhostname
echo This system has host name $(<myhostname)

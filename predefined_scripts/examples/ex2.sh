#!/bin/sh
# Find an entry in the password file
pwent=`grep '^root:' /etc/passwd`
# Turn off globbing - passwd lines often contain '*'
set -o noglob
# The "full name" and other comments are in
# field 5, colon delimited.  Get this field using shell word splitting
OIFS=$IFS; IFS=: ; set $pwent; IFS=$OIFS
echo $5

#!/bin/bash
# $Header: $
# First attempt at a consolidated auth log collection from kaserver
# Timestamps in the raw files are NOT designed for easy sorting.
#
# Options:
#  -i  -- translate hex IP addresses to dotted-decimal (relatively quick)
#  -h  -- translate hex IP addresses to DNS names (somewhat slower - DNS lookups)
#  -u user -- filter for the named user before translating addresses

hextodec()
{
   # convert the IP address in reverse-hex to dotted-decimal
   echo $((0x${1:6:2})).$((0x${1:4:2})).$((0x${1:2:2})).$((0x${1:0:2}))
}

hostlookup()
{
   # Convert a decimal IP to hostname - calls 'host' each time
   hostname=$(host $1)
   case $hostname in
   *\ not\ found*)
      # Just echo the address we tried to look up
      echo "$1"
      ;;
   *)
      # The result is word 5.  Lower-case it for consistency
      set $hostname
      echo "$5" | tr 'A-Z' 'a-z'
      ;;
   esac
}

# Options
iptranslate=0
gethostnames=0
filter=cat
while getopts ihu: o ; do
   case $o in
   i) iptranslate=1 ;;
   h) gethostnames=1; iptranslate=1 ;;
   u) filter="grep $OPTARG" ;;
   esac
done
shift $(($OPTIND-1))

# We could get the DB server names from 'fs checkservers', but it isn't obvious what is from our cell.  We
# could also grep CellServDB.  I cop out and hard code one known DB server and get the others from it.
masterserver=halley.dartmouth.edu
serverlist=$(bos listhosts -server $masterserver| grep 'Host .* is ' | awk '{print $4}')

# If we want to filter usernames, it is more efficient to do it inline, before sorting, translation and hostname lookups

# Array to hold IP address/name conversions (associative array, ksh only)
# ksh - use -A for associative array.  bash - use -a and numeric array
typeset -a hostnames

(
for dbserver in $serverlist; do
   bos getlog -server $dbserver -file /usr/afs/logs/AuthLog
done
) | grep -v 'Fetching log file' | $filter | sed -e 's/^... //' -e 's/  \([1-9]\) / 0\1 /' | sort --month-sort | \
    sed '-e s/ \([0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f]\)$/ 0\1/' |
    while read line; do
   if [[ $iptranslate == 1 ]] ; then
      # Ugly!
      # Sometimes we get a 7-digit hex code in the log - the kaserver apparently drops leading zeros.
      # The second 'sed' in the pipe catches these are fixes them.
      case $line in
      *\ from\ [0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f])
         # translate the reverse-hex address
         iphex=${line##* from }
         # bash version - index by numeric value only, but can be sparse array -- use the raw IP 
         ipdec=$((0x$iphex))
         frontpart=${line% from *}
         if [[ $gethostnames == 1 ]]; then
            # ksh - index on hex value as a string (iphex)
            # bash - index on numeric value (ipdec)
            index=$ipdec
            if [[ -z "${hostnames[$index]}" ]]; then
               hostnames[$index]="$(hostlookup $(hextodec $iphex))" 
            fi
            echo "$frontpart from ${hostnames[$index]}"
         else
            echo "$frontpart from $(hextodec $iphex)"
         fi
         ;;
      *)
         echo "$line"
         ;;
      esac
   else
      # No ip translation, just echo the whole line
      echo "$line"
   fi   
done


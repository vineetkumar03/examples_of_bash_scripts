#!/bin/sh
# Example 15

# Uses ex13.sh to generate some output and give us an 
# exit status to capture.

# Get the exit status of ex13 into $ex13stat.  
# stdout of ex13 is processed normally

# Save a copy of stdout
exec 3>&1
# Run a subshell, with 4 duplicated to 1 so we get it in stdout.  
# Capture the output in ``
# ex13stat=`( ...  ) 4>&1`
# Inside the subshell, run another subshell to execute ex13, 
# and echo the status code to 4
# (./ex13.sh; echo $? >&4)
# stdout from the inner subshell is processed normally, but the 
# subsequent output must be directed to 3 so it goes to the 
# original stdout and not be captured by the ``
ex13stat=`((./ex13.sh; echo $? >&4) | grep 'foo' 1>&3) 4>&1`

echo Last command status=$?
echo ex13stat=$ex13stat

# If any of the commands really care about inheriting open file 
# descriptors that they don't need then a more correct command line 
# closes the descriptors before running the commands
exec 3>&1
ex13stat=`((./ex13.sh 3>&- 4>&- ; echo $? >&4) | \
   grep 'foo'  1>&3 3>&- 4>&- ) 4>&1`
echo Last command status=$?
echo ex13stat=$ex13stat

#!/bin/sh
# Example 14
# Take stderr from a command and pass it into a pipe
# for further processing.

# Uses ex13.sh to generate some output to stderr
# stdout of ex13 is processed normally

# Save a copy of original stdout
exec 3>&1

# stdout from ex13.sh is directed to the original stdout (3)
# stderr is passed into the pipe for further processing.
# stdout from the pipe is redirected back to stderr
./ex13.sh 2>&1 1>&3 3>&-  | sed 's/stderr/STDERR/' 1>&2

# 3 is closed before running the command, just in case it cares
# about inheriting open file descriptors.

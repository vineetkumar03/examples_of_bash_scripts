#!/bin/ksh
# For the purposes of display, parts of the script have 
# been rendered in glorious technicolor.
## Some comments are bold to flag special sections

# Line numbers on the left are not part of the script.
# They are just added to the HTML for reference.

# Built-in commands and keywords (e.g. print) are in blue
# Command substitutions are purple. Variables are black
print "Disk usage summary for $USER on `date`"

# Everything else is red - mostly that is external 
# commands, and the arguments to all of the commands.
print These are my files       # end of line comment for print
# List the files in columns
ls -C
# Summarize the disk usage
print
print Disk space usage
du -k 
exit 0

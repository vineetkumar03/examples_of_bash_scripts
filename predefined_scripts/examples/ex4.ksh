#!/bin/ksh
count=0
max=10
while [[ $count -lt $max ]]
do 
  echo $count
  count=$((count + 1))
done
echo "Value of count after loop is: $count"

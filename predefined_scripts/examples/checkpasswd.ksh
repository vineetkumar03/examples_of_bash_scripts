#!/bin/ksh
# check that all home directories in the password file actually exist
# and that .forward files are present - create as needed
# RB 960424
#
# 981216 RB Added check for DND Affiliation field - now output by chkdnd
# 981222 RB Move to New Webster.  Commented out #### things that need fixing
# 990119 RB Change .forward files to be owned by the real users
# 990129 RB Start modifying to assume the DNDUID and DEPTCLASS will be in the GECOS field, comma separated
#        Bug/feature with usermod - won't replace GECOS with commas if there are already commas in the field.
# 991201 RB Added TEMPPASSWD
# 000119 RB Added $echotty (-e option) and put /var/adm/dart/accts in $PATH for chkdnd (use in crontab)
# 000301 RB Added check for over-quota as well as no-quota
# 000418 RB Convert to ksh to try and speed up a little.  Old version saved as checkpasswd.sh
#           Changed chkdnd and tee into coprocesses - read arguments from stdin as alternate to command line
# 010109 RB Change location to /usr/local/adm/accts

TEMPPASSWD=`basename $0`.passwd.local

# Explicitly set the PATH to make sure we can find everything (important for execution by
# cron)
PATH=/bin:/usr/bin:/usr/ucb:/tcb/bin:/usr/sbin:/usr/local/adm/accts
export PATH

# Functions for later
wordsplit()
{
  # Take a string in $1, a set of delimiters in $2, and print the token
  # indexed by $3
  # Set noglob in the calling routine to avoid expanding wildcards in the result
  typeset arg=$3
  IFS=$2 ; set $1
  eval print -r \$$arg
}

# Nothing in this script uses filename generation.  Turn off globbing to avoid problems
# expanding accidental wildcards in GECOS fields etc.
set -o noglob

echotty=
if [[ -t 0 || -t 1 ]] ; then
    # stdin and stdout cannot be default for this script - set them here
    if [[ -t 0 ]] ; then
       # NOTE - if /etc/passwd is modified by another process while we are running, this
       #        will mess up our stdin - subsequent reads may fail
       #        Use a local copy for safety, but don't modify that either!
       if [[ -e $TEMPPASSWD ]]; then
          # Bail - should never happen
          print "Error: $TEMPPASSWD already exists; bailing"
          exit 1
       fi
       print "Setting up input from a copy of /etc/passwd"
       cp /etc/passwd $TEMPPASSWD
       exec 0<$TEMPPASSWD
    fi
    if [[ -t 1 ]] ; then
       # Output is not redirected anywhere - set output to a file and screen
       print "Setting up output to tee into $0.out"
       tee $0.out >/dev/tty |&
       # Output from this coprocess should go to the screen
       # Our stdout all goes into this coprocess
       exec 1>&p 
    fi
    # If we reassign input/output this way, assume we also want to see the names echoed on screen,
    # so add the -e option to any others that we already have.
    echotty=1
fi 

verbose=0
for arg in $@; do
   case $arg in
   -e)
       echotty=1 ;;
   +e) 
       echotty= ;;
   -v)
       verbose=1 ;;
   -vv)
       verbose=2 ;;
   -vvv)
       verbose=3 ;;
   -f)
       # Set this variable to enable a rebuild of the personal .forward files
       rebuildforward=1
       ;;
   -c)
       # Set this to force a 'chown' and 'chmod' of all .forward files - don't need to do this normally
       rechownforward=1
       ;;
   -g)
       # Set this to force a chgrp of the regular user accounts
       rechgrphome=1
       ;;
   -d)
       # Special - rebuild the DNDUID and DEPTCLASS codes in /etc/passwd
       # To run this, we MUST operate on a copy of /etc/passwd since otherwise we would be modifying
       # our own input - do 'checkpasswd < /etc/passwd.copy > checkpasswd.out'
       rebuilddnduid=1
   esac
done

integer pwtotal=$(wc -l < /etc/passwd)
integer count=0
# start the co-process for looking up in DND
chkdnd |&
sleep 5
# move the input/output streams so we can close it later.
exec 4>&p
exec 5<&p
# open a stream on /dev/tty for additional diagnostics - faster than multiple redirects
[[ ! -z "$echotty" ]] && exec 6>/dev/tty

while read PWENT; do
   UNAME=$(wordsplit "$PWENT" ":" 1)
   UHOME=$(wordsplit "$PWENT" ":" 6)
   ERR=0
   AFFIL="unknown"
   OTHER=

   # This is just to give me warm fuzzies
   [[ ! -z "$echotty" ]] && print -u6 "($(((count+=1)*100/pwtotal))%) $UNAME           "\\r\\c
   # debugging - leave in the newlines
   #[[ ! -z "$echotty" ]] && print -u6 "($(((count+=1)*100/pwtotal))%) $UNAME           "

   if [[ -d "$UHOME" ]] ; then
      case $UHOME in
      */people/*)
         # PERSONAL Accounts
         ##DNDNAME=$(chkdnd $UNAME 2>/dev/null)
         print -u4 $UNAME
         read -u5 DNDNAME

         case $DNDNAME in
         Error:*)
            FULLNAME=$(wordsplit "$PWENT" ":" 5 | sed -e 's/,.*//' )
            MAILNAME=$(print $FULLNAME | sed -e 's/ /\./g' -e 's/,.*//' -e 's/\.\{1,\}/\./g')
            print "No DND entry for personal account $UNAME: $FULLNAME : $DNDNAME"
            ERR=1
            ;;
         *)
            FULLNAME=$(wordsplit "$DNDNAME" ":" 1)
            MAILNAME=$(print $FULLNAME | sed -e 's/ /\./g' -e 's/,.*//' -e 's/\.\{1,\}/\./g')
            AFFIL=$(wordsplit "$DNDNAME" ":" 2)
            # Check DND affiliation field - personal accounts should be DART with very few exceptions
            # Do this check here since DND-misses can't be checked.
            case $AFFIL in
            S-FAC|E-FAC|DART)
               ;;
            *)
               print "Warning: DND affiliation $AFFIL for personal account $UNAME"
               ;;
            esac
            ;;
         esac

         # special - rebuild .forward files for the personal accounts
         if [[ ! -z "$rebuildforward" ]]; then
            print "Recreating .forward in $UHOME for $FULLNAME"
            print ${MAILNAME}@dartmouth.edu > $UHOME/.forward
            chown $UNAME $UHOME/.forward
            chmod 644  $UHOME/.forward
         fi

         # special - chgrp "nobody" for all personal accounts that are FTP only
         if [[ ! -z "$rechgrphome" ]]; then
            case $PWENT in
            *nologin)
                print "chgrp -R nobody $UHOME for $FULLNAME"
                chgrp -R nobody $UHOME
                ;;
            *)
                # shell accounts and other specials, we skip
                print "skip chgrp -R nobody $UHOME for $FULLNAME - shell account"
                ;;
            esac
         fi

         if [[ ! -f $UHOME/.forward ]] ; then
             if [[ $ERR -eq 0 ]] ; then
                 # no .forward, but safe to guess one in /people
                 print "No .forward in $UHOME for $FULLNAME - creating"
                 print ${MAILNAME}@dartmouth.edu > $UHOME/.forward
                 chown $UNAME $UHOME/.forward
                 chmod 644  $UHOME/.forward
             else
                 # no .forward, and no DND entry to give us a full name
                 print "No .forward in $UHOME for $FULLNAME but cannot guess at address"
             fi
         else
             if [[ ! -z "$rechownforward" ]]; then
                chown $UNAME $UHOME/.forward
                chmod 644  $UHOME/.forward
             fi 
             if [[ "$verbose" -ge 2 ]] ; then
                 # check for potentially incorrect .forward files
                 [[ "`cat $UHOME/.forward|tr '[A-Z]' [a-z]'`" != "`echo ${MAILNAME}@dartmouth.edu|tr [A-Z] [a-z]`" ]] && \
                    printf "%9s %s\n" $UNAME: "forwarding to `cat $UHOME/.forward` not \"${MAILNAME}@dartmouth.edu\""
             fi
             [[ "$verbose" -ge 3 &&  -r $UHOME/.forward ]] && \
                 printf "%9s %s\n" $UNAME: "forwarding to `cat $UHOME/.forward`"
         fi
         # check quota
         uquot=$(quota $UNAME)
         case "$uquot" in
         *\**)
            print "Warning: exceeded quota for personal account $UNAME"
            ;;
         *none*)
            print "Warning: no quota for personal account $UNAME"
            ;;
         esac

         # Make sure the home directory is actually owned by the user
         # Note - if an account is disabled (flagged for deletion) then the ownership is always wrong
         if [[ "$(ls -ld $UHOME | awk '{print $3}')" != $UNAME ]]; then
            grep "^$UNAME:" /etc/passwd | grep -q disabled || \
                print "Warning: home directory for $UNAME has wrong ownership"
         fi
         # Check the permissions on the home directory
         case $(ls -ld $UHOME |cut -c8-10) in
         r-x)
            # normal
            ;;
         *w*)
            # Not good - should not be public write
            print "Warning: home directory for $UNAME is public writeable"
            ;;
         *-)
            # Probably not good, but may be intentional
            print "Warning: home directory for $UNAME is not public readable"
            ;;
         esac

         if [[ ! -z "$rebuilddnduid" ]]; then
            DNDUID=$(wordsplit "$DNDNAME" ":" 3)
            DEPTCLASS=$(wordsplit "$DNDNAME" ":" 4)
            # Get the 'other stuff' from an existing passwd field entry, if any
            OTHER=$(wordsplit "$PWENT" ":" 5 | cut -s -d, -f2-)
            case $OTHER in
            $DNDUID,$DEPTCLASS*) 
               # We already have this stuff in the entry- don't repeat it
               OTHER=$(print $OTHER | cut -d, -f3-)
               ;;
            esac
            GECOS="$FULLNAME,$DNDUID,$DEPTCLASS" 
            [[ ! -z "$OTHER" ]] &&  GECOS="$GECOS,$OTHER"

            # The object here is to get password GECOS fields into the form :full name,dnduid,deptclass,optional other stuff:
            # Note: usermod won't let us put in GECOS with commas unless we take out the previous content, also it
            # locks the account, so we have to explicitly unlock it 
            print "/usr/sbin/usermod -c \"$GECOS\" $UNAME"
            /usr/sbin/usermod -c " " $UNAME
            /usr/sbin/usermod -c "$GECOS" $UNAME
            /usr/sbin/usermod -x administrative_lock_applied=0 $UNAME
         fi
         ;;

      */data/public/*)
         # DEPARTMENT or ADMIN or ORGANIZATION or COURSES
         ##DNDNAME=$(chkdnd $UNAME 2>/dev/null)
         print -u4 $UNAME
         read -u5 DNDNAME

         case $DNDNAME in
         Error:*)
            FULLNAME=$(wordsplit "$PWENT" ":" 5 | sed -e 's/,.*//' )
            MAILNAME=$(print $FULLNAME | sed -e 's/ /\./g' -e 's/,.*//' -e 's/\.\{1,\}/\./g')
            print "No DND entry for dept/group/project account $UNAME: $FULLNAME : $DNDNAME"
            ERR=1
            ;;
         *) 
            FULLNAME=$(wordsplit "$DNDNAME" ":" 1)
            MAILNAME=$(print $FULLNAME | sed -e 's/ /\./g' -e 's/,.*//' -e 's/\.\{1,\}/\./g')
            AFFIL=$(wordsplit "$DNDNAME" ":" 2)
            # Check DND affiliation field - dept/group accounts
            # Do this check here since DND-misses can't be checked.
            case $UHOME in
            *courses*)
               # In the courses directories, we expect the owners to be people
               case $AFFIL in
               S-FAC|E-FAC|DART)
                  ;;
               *)
                  print "Warning: DND affiliation $AFFIL for Course account $UNAME"
                  ;;
               esac
               ;;
            *)
               # In any other directory, we expect most of the owners to be ORG or DEPT 
               case $AFFIL in
               ORG|DEPT)
                  ;;
               *)
                  # There are lots of these warnings, so do it only with verbose>0
                  [[ "$verbose" -ge 2 ]] && print "Warning: DND affiliation $AFFIL for Dept/Group/Project account $UNAME"
                  ;;
               esac
               ;;
            esac
         esac  

         if [[ ! -f $UHOME/.forward ]] ; then
            if [[ $ERR -eq 0 ]] ; then
                # no .forward for non-people accounts - do we want to guess or just report it ??
                print "No .forward in $UHOME for $FULLNAME - creating"
                print ${MAILNAME}@dartmouth.edu > $UHOME/.forward
                chown $UNAME $UHOME/.forward
                chmod 644  $UHOME/.forward
             else
                 # no .forward, and no DND entry to give us a full name
                 print "No .forward in $UHOME for $FULLNAME but cannot guess at address"
            fi
         else
            if [[ ! -z "$rechownforward" ]]; then
               chown $UNAME $UHOME/.forward
               chmod 644  $UHOME/.forward
            fi
            if [[ "$verbose" -ge 2 ]] ; then
               # check for potentially incorrect .forward files
               [[ "`cat $UHOME/.forward|tr '[A-Z]' [a-z]'`" != "`echo ${MAILNAME}@dartmouth.edu|tr [A-Z] [a-z]`" ]]\
               &&  printf "%9s %s\n" $UNAME: "forwarding to `cat $UHOME/.forward` not \"${MAILNAME}@dartmouth.edu\""
            fi

            [[ "$verbose" -ge 3 && -r $UHOME/.forward ]] && \
                printf "%9s %s\n" $UNAME: "forwarding to `cat $UHOME/.forward`"

         fi
         # check quota
         uquot=$(quota $UNAME)
         case "$uquot" in
         *\**)
            print "Warning: exceeded quota for dept/group/project account $UNAME"
            ;;
         *none*)
            print "Warning: no quota for dept/group/project account $UNAME"
            ;;
         esac

         # Make sure the home directory is actually owned by the user
         if [[ "$(ls -ld $UHOME | awk '{print $3}')" != $UNAME ]]; then
            echo Warning: home directory for $UNAME has wrong ownership
         fi
         # Check the permissions on the home directory
         case $(ls -ld $UHOME |cut -c8-10) in
         r-x)
            # normal
            ;;
         *w*)
            # Not good
            print "Warning: home directory for $UNAME is public writeable"
            ;;
         *-)
            # Probably not good, but may be intentional
            print "Warning: home directory for $UNAME is not public readable"
            ;;
         esac

         if [[ ! -z "$rebuilddnduid" ]]; then
            DNDUID=$(wordsplit "$DNDNAME" ":" 3)
            DEPTCLASS=$(wordsplit "$DNDNAME" ":" 4)
            # Get the 'other stuff' from an existing passwd field entry, if any
            OTHER=$(wordsplit "$PWENT" ":" 5 | cut -s -d, -f2-)
            case $OTHER in
            $DNDUID,$DEPTCLASS*)
               # We already have this stuff in the entry- don't repeat it
               OTHER=$(print $OTHER | cut -d, -f3-)
               ;;
            esac
            GECOS="$FULLNAME,$DNDUID,$DEPTCLASS"
            [[ ! -z "$OTHER" ]] &&  GECOS="$GECOS,$OTHER"

            # The object here is to get password GECOS fields into the form :full name,dnduid,deptclass,optional other stuff:
            # Note: usermod won't let us put in GECOS with commas unless we take out the previous content, also it
            # locks the account, so we have to explicitly unlock it 
            print "/usr/sbin/usermod -c \"$GECOS\" $UNAME"
            /usr/sbin/usermod -c " " $UNAME
            /usr/sbin/usermod -c "$GECOS" $UNAME
            /usr/sbin/usermod -x administrative_lock_applied=0 $UNAME
         fi
         ;;

      *)
          # non-web accounts, definitely don't guess on a .forward, and don't look up in DND
          FULLNAME=$(print $PWENT | cut -d: -f5 | sed -e 's/,.*//' )
          [[ "$verbose" -ge 2 && ! -f $UHOME/.forward  ]] && print "No .forward in $UHOME for $FULLNAME"
          [[ "$verbose" -ge 3 && -r $UHOME/.forward ]] && printf "%9s %s\n" $UNAME: "forwarding to `cat $UHOME/.forward`"
          ;;
      esac
   else
      # no home - fix this first before checking further
      print "Error: non-existant home directory for $UNAME: $UHOME"
   fi
   
   # check authentication files for all users
   # Version for DU 4.x
   # edauth -gq outputs nothing if the entry is OK, else an error message
   /tcb/bin/edauth -gq $UNAME 2>/dev/null || echo  Warning: no authentication database entry for $UNAME
done

# close the co-process to make it go away
exec 4>&-

# Further checks on the password file taken as a whole
# Look for people with multiple personal logins. 
# Checking for multiple logins in total brings up many instances of departmental+personal, having the same
# entry in the GECOS field

print
print "Checking for multiple personal logins:"
print
grep 'people' /etc/passwd | awk -F: '{print $5}' | sort | uniq -d | while read user; do
   grep ":$user.*people" /etc/passwd
done

print
print "Checking for multiple logins with same UID:"
print
awk -F: '{print $3}' /etc/passwd | sort | uniq -d | while read uid ; do
   grep ":$uid:" /etc/passwd
done

print "Running authck:"
print
/tcb/bin/authck -p

rm -f $TEMPPASSWD


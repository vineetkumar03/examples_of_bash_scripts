#!/bin/bash
# Try this under bash, ksh and sh

trap huphandler  HUP
trap ''          QUIT
trap exithandler TERM INT

huphandler()
{
   echo 'Received SIGHUP'
   echo "continuing"
}

exithandler()
{
   echo 'Received SIGTERM or SIGINT'
   exit 1
}
## Execution starts here - infinite loop until interrupted
# Use ":" or "true" for infinite loop
# SECONDS is built-in to bash and ksh.  It is number of seconds since script started
: is like a comment, but it is evaluated for side effects and evaluates to true
seconds=0
while : ; do
# while true; do
   sleep 5
   seconds=$((seconds + 5))
   echo -n "$SECONDS $seconds - "
done
